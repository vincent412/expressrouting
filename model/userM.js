const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    isBanned: {
        type: Boolean,
        default: false
    },
    roles: {
        type: [String],
        default: ['user'] // admin
    },
    interfaceSettings: {
        dark: {
            type: Boolean,
            default: true
        },
        toolbarColor: {
            type: String,
            default: '#FFA809'
        }
    },
    activAccount: {
        ref: 'Account',
        type: Schema.Types.ObjectId,
    }
}, { timestamps: true })



module.exports = mongoose.model('User', userSchema)
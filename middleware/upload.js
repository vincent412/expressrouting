const multer = require('multer')
const moment = require('moment')
const fs = require('fs');
const path = require('path')

const storage = multer.diskStorage({
    destination(req, file, cb) {
        const uploadPath = path.join('public', 'images', 'uploads')
        fs.exists(uploadPath, (exists) => {
            if (!exists) {
                fs.mkdir(uploadPath, { recursive: true }, (err) => {
                    if (err) throw err;
                })
            }
        });

        cb(null, uploadPath)
    },
    filename(req, file, cb) {
        const date = moment().format('YYYYMMDD-HHmmss_SSS')
        cb(null, `${date}-${file.originalname}`)
    }
})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'application/pdf') {
        cb(null, true)
    } else {
        cb(null, false)
    }
}

const limits = {
    fileSize: 1024 * 1024 * 5
}


module.exports = multer({ storage, fileFilter, limits })
var express = require('express');
var upload = require('../../../middleware/upload');
var router = express.Router();

// simple upload
// const multer = require('multer')
// const uploadS = multer({ dest: 'public/uploads/images/simple' });
var multer = require('multer');
var path = require('path')

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'public/images/uploads/')
	},
	filename: function (req, file, cb) {
		cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
	}
})

var uploadS = multer({ storage: storage });


const usersRouter = require('./usersRouter')
var indexController = require('../../../controller/v1/indexController');
var fsController = require('../../../controller/v1/fsController');


/* GET home page. */
router.get('/', (req, res) => {
	res.render('index', { title: 'Express API' });
});
router.use('/users', usersRouter);
router.get('/upload', indexController.index)
router.post('/upload', upload.single('myFile'), indexController.store)
router.get('/uploads', indexController.indexArray)
router.post('/uploads', upload.any(), indexController.storeArray)

router.get('/simple', indexController.simpleUpload)
router.post('/simple', uploadS.single('myFile'), indexController.storeSimpleUpload)

router.get('/fs', fsController.index)

module.exports = router;
var express = require('express');
var router = express.Router();
const usersController = require('../../../controller/v1/usersController')

/* GET home page. */
router.get('/', usersController.index);
router.get('/show/:_id', usersController.show);

module.exports = router;

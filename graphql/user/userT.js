module.exports = `
type User {
    _id: ID,
    username: String
    email: String!
    password: String!
    isBanned: Boolean
    roles: [String]
    interfaceSettings: InterfaceSettings
    
    
    createdAt: String!
    updatedAt: String!
}

type InterfaceSettings {
    dark: Boolean,
    toolbarColor: String
}

type Query {
    user: QueryUser
}

type QueryUser {
    index: [User]
    show(_id: ID): User
}

type Mutation {
    user: MutationsUser
}

type MutationsUser{
    store( input: InputUser) : PayloadUser
    update( _id: ID, input: InputUser): PayloadUser
}
    input InputUser {
        username: String
        email: String
        activAccount: ID
        interfaceSettings: InputInterfaceSettings
    }
    input InputInterfaceSettings {
        dark: Boolean,
        toolbarColor: String
    }
    type PayloadUser {
        record: User
        recordId: ID
        query: Query
    }
    type PayloadInterface {
        record: User
        recordId: ID
        query: Query
    }

type Subscription {
    user: SubscriptionUser
}

type SubscriptionUser {
    count: Int!
}
`
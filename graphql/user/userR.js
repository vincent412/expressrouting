const User = require('../../model/userM')


module.exports = {
    User: {
        password: source => ``,
    },
    Query: {
        user: (_, __, ctx) => ({})
    },

    QueryUser: {
        index: async () => {
            return await User.find()
        },
        show: async (source, { _id }, ctx, info) => {
            return await User.findOne({ _id })
        },
    },

    Mutation: {
        user: () => ({})
    },
    Subscription: {
        user: () => ({})
    },
    SubscriptionUser: {
        count: {
            subscribe(parent, args, { pubsub }, info) {
                let count = 0

                setInterval(() => {
                    count++
                    pubsub.publish('count', {
                        count
                    })
                }, 1000)

                return pubsub.asyncIterator('count')
            }
        }
    },

    MutationsUser: {
        store: async (source, { input }, contex, info) => {
            const user = await User.create(input)
            return {
                record: user,
                query: () => ({}),
            }
        },
        update: async (source, { _id, input }, ctx, info) => {
            const data = Object.assign({}, input)

            delete data.interfaceSettings
            const { interfaceSettings } = input
            if (interfaceSettings !== undefined) {
                if (interfaceSettings) {
                    if (interfaceSettings.dark !== undefined) data['interfaceSettings.dark'] = interfaceSettings.dark
                    if (interfaceSettings.toolbarColor !== undefined) data['interfaceSettings.toolbarColor'] = interfaceSettings.toolbarColor
                }
            }

            const user = await User.findOneAndUpdate(_id,
                { $set: data },
                { new: true }
            )


            return {
                record: user,
                recordId: user._id,
                query: () => ({}),
            }
        }
    }
}
// schema.js
// graphql-tools merge-graphql-schemas
const { makeExecutableSchema } = require('graphql-tools')
const { mergeTypes, mergeResolvers } = require('merge-graphql-schemas');

const userR = require('./user/userR')
const userT = require('./user/userT')


const resolvers = mergeResolvers([
    userR,
])

const typeDefs = mergeTypes([
    userT
]);
const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

module.exports = schema
const User = require('../../model/userM')
module.exports.index = async (req, res) => {
    const users = await User.find()
    console.log(users);
    res.render('users/index', { users: users })
}
module.exports.show = async (req, res) => {

    const user = await User.findOne({ _id: req.params._id })
    console.log(user);
    res.render('users/show', { user: user })
}
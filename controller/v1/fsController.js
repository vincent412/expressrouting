const fs = require('fs')
module.exports.index = (req, res) => {
    const fsa = fs.readdirSync(__basedir)

    res.render('fileSystem/index', { title: 'Express FileSystem', dir: fsa, path: __dirname });
}
// const resize = require('../../helper/imageResizer')



// module.exports.index = (req, res) => {

//     const widthString = req.query.width
//     const heightString = req.query.height
//     const format = req.query.format
//     let width, height
//     if (widthString) {
//         width = parseInt(widthString)
//     }
//     if (heightString) {
//         height = parseInt(heightString)
//     }

//     res.type(`image/${format || 'png'}`)
//     resize(__dirname + '/bau.jpg', format, width, height).pipe(res)

// }
//requiring path and fs modules
const path = require('path');
const fs = require('fs');
//joining path of directory 


module.exports.index = (req, res) => {

    res.render('upload/upload')
}

module.exports.store = (req, res) => {
    if (req.file) {
        console.log('Uploading file...');
        var filename = req.file.filename;
        var uploadStatus = 'File Uploaded Successfully';
    } else {
        console.log('No File Uploaded');
        var filename = 'FILE NOT UPLOADED';
        var uploadStatus = 'File Upload Failed';
    }

    /* ===== Add the function to save filename to database ===== */

    res.render('upload/upload', { status: uploadStatus, filename: filename });
}
module.exports.indexArray = (req, res) => {
    const directoryPath = path.join(__basedir, 'public', 'images', 'uploads');
    //passsing directoryPath and callback function
    let data = []
    fs.readdir(directoryPath, function (err, files) {
        //handling error
        if (err) {
            console.log('Unable to scan directory: ' + err);
            return []
        }
        //listing all files using forEach
        let data = files.map(function (file) {
            // Do whatever you want to do with the file
            return { filename: file };
        });
        console.log(data, 'files');
        res.render('upload/uploads', { filename: data })
    });


}
module.exports.storeArray = (req, res) => {
    if (req.files) {
        console.log('Uploading file...');
        var filename = req.files
        var uploadStatus = 'File Uploaded Successfully';
    } else {
        console.log('No File Uploaded');
        var filename = 'FILE NOT UPLOADED';
        var uploadStatus = 'File Upload Failed';
    }

    /* ===== Add the function to save filename to database ===== */

    res.render('upload/uploads', { status: uploadStatus, filename: filename });
}


module.exports.simpleUpload = (req, res) => {
    res.render('upload/simple')
}
module.exports.storeSimpleUpload = (req, res) => {
    if (req.file) {
        console.log('Uploading file...');
        var filename = req.file.filename;
        var uploadStatus = 'File Uploaded Successfully';
    } else {
        console.log('No File Uploaded');
        var filename = 'FILE NOT UPLOADED';
        var uploadStatus = 'File Upload Failed';
    }

    /* ===== Add the function to save filename to database ===== */

    res.render('upload/upload', { status: uploadStatus, filename: filename });
}
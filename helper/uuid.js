const crypto = require('crypto')

module.exports = function hash() {
    text = (1 + Math.random() * new Date().getTime()).toString(16),
        key = (1 + Math.random() * new Date().getTime() * 12345).toString(16)

    // create hahs
    const hash = crypto.createHmac('sha512', key)
    hash.update(text)
    return hash.digest('hex')
}
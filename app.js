var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var { ApolloServer, PubSub } = require('apollo-server-express');

var apiRouter = require('./routes/api/v1/indexRouter');

var app = express();

// Global
global.__basedir = __dirname

// view engine setup
var hbs = require('hbs');
hbs.registerPartials(__dirname + '/views/partials');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// DB Connect
require('./helper/connector')

// gql
const pubsub = new PubSub()
const schema = require('./graphql/index')
const server = new ApolloServer({
  schema,
  context: { pubsub },
});
server.applyMiddleware({
  app,
  path: '/graphql'
})

// Router
app.use('/', apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;

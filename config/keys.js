module.exports = {
    //APP
    APP_NAME: 'TopRechnung',
    APP_URL: process.env.NODE_ENV == "production" ? 'https://app.toprechnung.de' : 'http://localhost:8080',
    // APP_URL: 'http://localhost:8080',  //IMPORTANT ohne / am ende
    // APP_URL: 'http://app.toprechnung.de',  //IMPORTANT ohne / am ende
    APP_COPYRIGHT: 'erstellt mit www.toprechnung.de', // wird in PDF verwendet

    //DB
    // mongoURL: 'mongodb://localhost:27017/test',
    mongoURL: 'mongodb://vincent4:vincent1@ds143242.mlab.com:43242/bf-mongo',
    JWT_SECRET_KEY: 'blackfocussoftware',

    // Email env
    transporter: {
        host: 'toprechnung.de',
        port: 25,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'info@toprechnung.de', // generated ethereal user
            pass: 'almanah1' // generated ethereal password
        },
        tls: {
            // do not fail on invalid certs
            rejectUnauthorized: false
        }
    }
}